//
//  UIConst.swift
//  BankaApplication
//
//  Created by Uros Rupar on 2/10/23.
//

import Foundation

struct UIConst {
    static let kFontJuaRegularName: String = "Jua"
    static let kFontPoppinsBoldName: String = "Poppins"
    static let kIBMPlexMonoSemiBold: String = "IBMPlexMono-SemiBold"
    static let kExpandInfoCollapsedFormatNameFontSize: CGFloat = 30
    static let kregularTextFontSize: CGFloat = 14
    static let kConditionsLabelSize: CGFloat = 11
    static let kCornerRadius: CGFloat = 7
    static let kBorderWidth: CGFloat = 2
    static let kLoginLabel: CGFloat = 25
    static let kBalanceLabel: CGFloat = 14
    
    struct CardCell {
        static let kSmallStrings:CGFloat = 14
        static let kMediumStrings:CGFloat = 18
        static let kLargeStrings:CGFloat = 25
    }
}
