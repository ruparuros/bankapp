//
//  Utils.swift
//  BankaApplication
//
//  Created by Uros Rupar on 2/10/23.
//

import Foundation
import UIKit

class Utils {
    
    static let APP_VERSION = infoForKey("APP_VERSION")
    
    
    fileprivate static func infoForKey(_ key: String) -> String? {
        let string = (Bundle.main.infoDictionary?[key] as? String)?.replacingOccurrences(of: "\\", with: "")
        return (Bundle.main.infoDictionary?[key] as? String)?.replacingOccurrences(of: "\\", with: "")
    }
    
   static func getRandomNumber(digits:Int) -> String {
        var number = String()
        for _ in 1...digits {
           number += "\(Int.random(in: 1...9))"
        }
        return number
    }
}
