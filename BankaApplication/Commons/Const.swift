//
//  Const.swift
//  BankaApplication
//
//  Created by Uros Rupar on 2/10/23.
//

import Foundation

struct Const {
    static let kDateFormatWithSlashesName = "EEE dd MMM yyyy"
}
