//
//  HomeViewController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/3/23.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setBackgroundGrayLight()
        setNavigaton()
        let user = DataManager.fetchData()
        let userService = UserService.shared
        userService.login(user: user)
        self.navigationItem.title = "APP NAME".localized()
        ExchangeManager.fetchConfig(url: "https://api.currencyapi.com/v3/currencies?apikey=cur_live_GjEuHd7tGT4EW1AJyTfUdnXaQre3fKi9XrV6R34z&currencies=EUR%2CUSD%2CCAD%2CAFN%2CERN", type: Currencies.self) {currencies in
            ExchangeViewController.currenciesArry = currencies
       // https://api.currencyapi.com/v3/historical?apikey=cur_live_GjEuHd7tGT4EW1AJyTfUdnXaQre3fKi9XrV6R34z&currencies=EUR%2CUSD%2CCAD&date=2023-10-10
        }
    }
}
