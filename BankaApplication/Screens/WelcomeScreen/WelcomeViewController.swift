//
//  ViewController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 2/3/23.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var versionLabel: UILabel!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var centralTitle: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    var currentQuoteIndex = 0 {
        didSet {
            self.descriptionLabel.text = WelcomeViewModel.quotes[self.currentQuoteIndex]
            
        }
    }
    var quotes:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUI()
        setTimer()
    }
    
    func setUI() {
        self.view.backgroundColor = .backgroungGreen
        UITextField.appearance().font = UIFont(name: "UIConst.kFontPoppinsBoldName", size: 13)
        self.navigationController?.navigationBar.tintColor = .white
        versionLabel.text = Utils.APP_VERSION
        versionLabel.styleForFontJuaRegular(fontSize: 10)
        versionLabel.textColor = .white
        centralTitle.text = "welcome".localized()
        centralTitle.styleForExpandInfoFormat()
        descriptionLabel.styleForRegularText()
        descriptionLabel.text = WelcomeViewModel.quotes[currentQuoteIndex]
        nextButton.styleForMainButton(title: "Next")
        dateLabel.text = Date().asString(format: Const.kDateFormatWithSlashesName)
        dateLabel.styleForFontJuaRegular(fontSize: 12)
        dateLabel.textColor = .white
    }
    
    func setTimer() {
        Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true) { _ in
            if self.currentQuoteIndex < WelcomeViewModel.quotes.count - 1 {
                self.currentQuoteIndex += 1
            } else {
                self.currentQuoteIndex = 0
            }
        }
    }
    
    @IBAction func goToLoginScreen(_ sender: Any) {
        let vc = LoginViewController.instantiate(loginViewModel: LoginViewModel())
        navigationController?.pushViewController(vc, animated: true)
    }
}

