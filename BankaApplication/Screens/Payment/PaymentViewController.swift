//
//  PaymentViewController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 10/27/23.
//

import UIKit

class PaymentViewController: UIViewController {

    @IBOutlet weak var mainstack: UIStackView!
    @IBOutlet weak var selectAccount: DropDownMenuView!
    
    @IBOutlet weak var selectPaymentReason: DropDownMenuView!
    
    @IBOutlet weak var inputRecieverName: DropDownMenuView!
    @IBOutlet weak var inputRecieverCity: DropDownMenuView!
    @IBOutlet weak var inputRecieverAddress: DropDownMenuView!
    @IBOutlet weak var inputAmount: DropDownMenuView!
    @IBOutlet weak var inputReceiverNumber: DropDownMenuView!
    @IBOutlet weak var inputModel: DropDownMenuView!
    @IBOutlet weak var inputReferenceNumber: DropDownMenuView!
    
    var selectedAccount: Account!
    var selectedPaymentReason: Reason!
    var receicerName: String!
    var receicerAddress: String!
    var receicerCity: String!
    var amount: String!
    var model: String!
    var receicerNumber: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedAccount = UserService.shared.currentUser?.accounts.first
        selectedPaymentReason = Reason.reason200
        selectAccount.delegate = self
        selectPaymentReason.delegate = self
        setUI()
    }
  
    
    func setUI() {
        mainstack.subviews(of: UITextField.self).forEach { textField in
            textField.delegate = self
        }
        setNavigaton()
        setBackgroundGrayLight()
        title = "paymentScreenTitle".localized()
        selectAccount.setMenuAccounts(placeholderText: selectedAccount.title ?? "", accounts: UserService.shared.currentUser.accounts, amount: ("\(selectedAccount.balance ?? 0.0) \(selectedAccount.currency.sign ?? "")"))
        inputRecieverName.setTextFieldType(placeholder: "Reciever name")
        selectPaymentReason.setMenu(placeholderText: "\(selectedPaymentReason.reasonTuple.0)  \(selectedPaymentReason.reasonTuple.1)", titles: Reason.allCases.map({ reason in
            "\(reason.reasonTuple.0)  \(reason.reasonTuple.1)"
        }))
        inputModel.setTextFieldType(placeholder: "model")
        inputAmount.setTextFieldType(placeholder: "amount")
        inputRecieverCity.setTextFieldType(placeholder: "receiver city")
        inputRecieverAddress.setTextFieldType(placeholder: "receiver address")
        inputReceiverNumber.setTextFieldType(placeholder: "receiver number")
        
    }
}

extension PaymentViewController: DropDownMenuViewDelegate {

    func onSelectedOrEnteredValue<T>(dropDownMenuView: DropDownMenuView, item: T, index: Int?){
        if dropDownMenuView == selectAccount {
            guard let account = item as? Account else { return }
            selectedAccount = account
            print(selectedAccount.title)
        }
       else if dropDownMenuView == selectPaymentReason {
           
            selectedPaymentReason = Reason.allCases[index!]
            print(selectedPaymentReason.reasonTuple.1)
        }
    }
    
    
}

extension PaymentViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
