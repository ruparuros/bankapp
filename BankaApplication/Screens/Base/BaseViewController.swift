//
//  BaseViewController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/19/23.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        
        // Do any additional setup after loading the view.
    }
    
    func setUI(){
        self.view.backgroundColor = .backgroungGreen
        UITextField.appearance().font = UIFont(name: "UIConst.kFontPoppinsBoldName", size: 13)
        self.navigationController?.navigationBar.tintColor = .white
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
