//
//  VerificationViewController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 2/24/23.
//

import UIKit
import UserNotifications

class VerificationViewController: UIViewController, UNUserNotificationCenterDelegate {
    @IBOutlet weak var sendVerificationCode: UIButton!
    @IBOutlet weak var checkverificatiopnCode: UIButton!
    @IBOutlet weak var verificationCodeTextField: UITextField!
    @IBOutlet weak var verificationCodeLabel: UILabel!
    @IBOutlet weak var randomTextLabel: UILabel!
    
    var verificationViewModel: VerificationViewModelProtocol!
    
    class var identifier: String { "VerificationViewController" }
    
    class func instantiate(verificationViewModel: VerificationViewModelProtocol) -> VerificationViewController {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        
        let vc = storyboard.instantiateViewController(identifier: identifier) as! VerificationViewController
        vc.verificationViewModel = verificationViewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.verificationViewModel.rendVerificationCode()
    }
    
    @IBAction func renderVerificationCode(_ sender: Any) {
        self.verificationViewModel.rendVerificationCode()
    }
    
    @IBAction func checkVerificationCode(){
        if let verificationCodeInput = verificationCodeTextField.text {
            if verificationCodeInput == self.verificationViewModel.verificationCode {
                let vc = MainTabBarController.instantiate()
                vc.modalPresentationStyle = .overCurrentContext
                self.present(vc, animated: true, completion: nil)
            } else {
                showAlert(title: "wrong code title".localized(), message: "wrong code message".localized())
            }
        }
    }
    
    func setUI(){
        self.view.backgroundColor = .backgroungGreen
        UITextField.appearance().font = UIFont(name: "UIConst.kFontPoppinsBoldName", size: 13)
        self.navigationController?.navigationBar.tintColor = .white
        checkverificatiopnCode.styleForMainButton(title: "Verify")
        checkverificatiopnCode.setTitle("Verify", for: .normal)
        verificationCodeLabel.styleForLoginLabel(title: "Verify")
        randomTextLabel.styleForRegularText()
        randomTextLabel.text = "random text verification".localized()
        verificationCodeTextField.setLoginFields()
        verificationCodeTextField.setPlaceHolderText(placeholder: "verificaton code".localized())
        sendVerificationCode.styleForResendButton(title: "resend verificaton code".localized())
    }
    
    
}

