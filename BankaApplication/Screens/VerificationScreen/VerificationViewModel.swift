//
//  VerificationViewModel.swift
//  BankaApplication
//
//  Created by Uros Rupar on 10/18/23.
//

import Foundation
import UIKit

protocol VerificationViewModelProtocol {
    var verificationCode:String! { get set }
    func rendVerificationCode()
}

class VerificationViewModel: VerificationViewModelProtocol {
    
    init() {
        center.requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in
        }
    }
    var verificationCode:String!
    let center = UNUserNotificationCenter.current()
    
    
     func rendVerificationCode(){
        let alarmNotification: UNMutableNotificationContent = UNMutableNotificationContent()
        alarmNotification.title = "verificaton code".localized()
        self.verificationCode = Utils.getRandomNumber(digits: 6)
        self.verificationCode = "1"
        if let verificationCode = self.verificationCode{
            alarmNotification.body = "\("verification code message".localized()) \(verificationCode)"
        }
        
        alarmNotification.categoryIdentifier = "myDemoCategory"

        let now = Date().addingTimeInterval(2)
        let triggerWeekly = Calendar.current.dateComponents([.weekday,.hour,.minute,.second], from: now)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: false)
        let request = UNNotificationRequest(identifier: "TestNotification\(now)", content: alarmNotification, trigger: trigger)

        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                  print("Uh oh! We had an error: \(error)")
            }
        }
    }
}
