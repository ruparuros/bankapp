//
//  TransactionViewModel.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/27/23.
//

import Foundation

class TransactionViewModel {
    
    static let shared = TransactionViewModel()
   
    private init(){
    }
    
    let userService = UserService.shared
    func getAcounts() -> [Account]?{
        return userService.getCurrentUser()?.accounts
    }
    
}
