//
//  TransactionViewController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/27/23.
//

import UIKit

class TransactionViewController: UIViewController {
    @IBOutlet weak var transactionTableView: UITableView!
    var accounts :[Account]!
    var transactionViewModel = TransactionViewModel.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        self.transactionTableView.delegate = self
               self.transactionTableView.dataSource = self

        
        let nib = UINib(nibName: "TransactionsCell", bundle: nil)
        transactionTableView.register(nib, forCellReuseIdentifier: "TransactionsCell")
        accounts = self.transactionViewModel.getAcounts()
        setNavigaton()
        seBackgroundGrayLight()
    }
    
     func seBackgroundGrayLight() {
        self.view.backgroundColor = .backgroungGraylightt
         title = "transaction title".localized()

        transactionTableView.sectionHeaderTopPadding = 0
        
        transactionTableView.tableHeaderView =  UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0.1))
    }

}

extension TransactionViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return accounts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return accounts?[section].transactions.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = transactionTableView.dequeueReusableCell(withIdentifier: "TransactionsCell") as? TransactionsCell
        
        cell?.createCell(transaction: accounts[indexPath.section].transactions[indexPath.row],currency: accounts[indexPath.section].currency)
        return cell ?? UITableViewCell()
        
    }
    
}

extension TransactionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = AccountTableHeaderView()
        view.createHeaderView(account: accounts[section])
        return view
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 85
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = TransactionDetailsViewController.instantiate(transaction: accounts[indexPath.section].transactions[indexPath.row], userService: UserService.shared
        )
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


