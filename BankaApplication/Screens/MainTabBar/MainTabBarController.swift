//
//  MainTabBarController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/24/23.
//

import UIKit

class MainTabBarController: UITabBarController {

    class var identifier: String { "MainTabBarController" }
     
     class func instantiate() -> MainTabBarController {
         let storyboard = UIStoryboard(name: "Home", bundle: nil)
         
         let vc = storyboard.instantiateViewController(identifier: identifier) as! MainTabBarController
         return vc
     }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        // Do any additional setup after loading the view.
    }
    
    func setUI(){
        UITabBar.appearance().barTintColor = .backgroungGreen
        tabBar.isTranslucent = false
        tabBar.tintColor = .white
        tabBar.unselectedItemTintColor = .systemGray4
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
