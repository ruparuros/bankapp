//
//  AccountViewController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 10/19/23.
//

import UIKit

class AccountViewController: UIViewController {
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var firstNameValueLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var lastNameLabelValue: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var dateOfBirthvalueLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordValueLabel: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    var userService: UserService!
    
    class var identifier: String { "AccountViewController" }
    
    class func instantiate(userService: UserService) -> AccountViewController {
        
        let storyboard = UIStoryboard(name: "Account", bundle: nil)
        
        let vc = storyboard.instantiateViewController(identifier: identifier) as! AccountViewController
        vc.userService = userService
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        
    }
    
    func setUI() {
        self.view.backgroundColor = .backgroungGraylightt
        usernameLabel.text = userService.currentUser?.fisrtName
        usernameLabel.styleForFontJuaRegular(fontSize: 25)
        firstNameLabel.styleForIBMPlexMonoSemiBold(fontSize: UIConst.CardCell.kSmallStrings)
        firstNameValueLabel.styleForFontJuaRegular(fontSize: UIConst.CardCell.kMediumStrings)
        firstNameValueLabel.text = userService.currentUser?.fisrtName
        
        lastNameLabel.styleForIBMPlexMonoSemiBold(fontSize: UIConst.CardCell.kSmallStrings)
        lastNameLabelValue.styleForFontJuaRegular(fontSize: UIConst.CardCell.kMediumStrings)
        lastNameLabelValue.text = userService.currentUser?.lastName
        
        dateOfBirthLabel.styleForIBMPlexMonoSemiBold(fontSize: UIConst.CardCell.kSmallStrings)
        dateOfBirthvalueLabel.styleForFontJuaRegular(fontSize: UIConst.CardCell.kMediumStrings)
        dateOfBirthvalueLabel.text = userService.currentUser?.birthDay.asString(format: "dd MM yyyy")
        
        passwordLabel.styleForIBMPlexMonoSemiBold(fontSize: UIConst.CardCell.kSmallStrings)
        passwordValueLabel.styleForFontJuaRegular(fontSize: UIConst.CardCell.kMediumStrings)
        passwordValueLabel.text = userService.currentUser?.password.generateSecureText()
        logoutButton.styleForLogoutButton(title: "logout".localized())
        
    }
}
