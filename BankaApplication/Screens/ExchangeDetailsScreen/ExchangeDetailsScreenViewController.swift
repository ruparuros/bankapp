//
//  ExchangeDetailsScreenViewController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 8/11/23.
//

import UIKit

struct Exchange {
    var valute: CAD
    var amount: Double
    
    var amountStringValue: String {
        return String(amount)
    }
}

class ExchangeDetailsScreenViewController: UIViewController {

    class var identifier: String { "ExchangeDetailsScreenViewController" }
    var exchangeDetailsViewModel: ExchangeDetailViewModel!
    var selectedDate: Date?
    var selectedValute: CAD?
    var valutesString: [String] {
        return ExchangeViewController.currenciesArry.values.filter { $0.code != selectedValute?.code
        }.map { cad in
            return cad.code ?? ""
        }
    }

    var exchanges: [ValuteValue]?
    var filteredExchanges: [ValuteValue]?
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var selectedValuteLabel: UILabel!
    @IBOutlet weak var exchangeTableView: UITableView!
   
    
    func generateUrl(baseVallute:String, othersValute:[String]) -> String{
        var valutes = ""
        
        for (index,item) in othersValute.enumerated() {
            valutes += item
            if index != othersValute.count - 1 {
                valutes += "%2C"
            }
        }
        
        return valutes
    }
    
    class func instantiate(exchangeDetailsViewModel: ExchangeDetailViewModel, selectedDate: Date, selectedValute: CAD) -> ExchangeDetailsScreenViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! ExchangeDetailsScreenViewController
        vc.selectedDate = selectedDate
        vc.selectedValute = selectedValute
        vc.exchangeDetailsViewModel = exchangeDetailsViewModel
        print(vc.generateUrl(baseVallute: vc.selectedValute?.code ?? "" ,othersValute: vc.valutesString))
        var url = "https://api.currencyapi.com/v3/historical?apikey=cur_live_GjEuHd7tGT4EW1AJyTfUdnXaQre3fKi9XrV6R34z&currencies=\(vc.generateUrl(baseVallute: vc.selectedValute?.code ?? "" ,othersValute: vc.valutesString))&base_currency=\(selectedValute.code ?? "EUR")&date=\(vc.selectedDate!.asString(format: "yyyy-MM-dd"))"
        print(url)
        ExchangeManager.fetchConfig(url: url, type: ConvertCurrenciy.self) {exchanges in
            vc.exchanges = exchanges?.values
            vc.filteredExchanges = exchanges?.values
            DispatchQueue.main.async {
                vc.exchangeTableView.reloadData()
              }
        }
        return vc
     }

    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "ExchaneDetailsTableViewCell", bundle: nil)
        exchangeTableView.register(nib, forCellReuseIdentifier: "ExchaneDetailsTableViewCell")
        selectedValuteLabel.text = "\(self.selectedValute?.code ?? "") \(self.selectedValute?.symbol ?? "")"
        selectedValuteLabel.textColor = .backgroungGreen
        exchangeTableView.sectionHeaderTopPadding = 0
        exchangeTableView.bounces = false
        exchangeTableView.separatorStyle = .none
        amountTextField.setInputAmountField()
        amountTextField.text = "1"
        setBackgroundGrayLight()
        setNavigaton()
        title = selectedValute?.name
        filteredExchanges = exchanges
    }
    @IBAction func textFieldChangdNumber(_ sender: UITextField) {
        filteredExchanges = exchanges
        if let number = Double(sender.text ?? "1") {
            filteredExchanges = exchanges!.map{ valute in
                var calculatedAmount = 0.0
                calculatedAmount = valute.value * number
                let object = ValuteValue(code: valute.code, value: calculatedAmount)
                return object
            }
            self.exchangeTableView.reloadData()
        }
    }
}

extension ExchangeDetailsScreenViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        exchanges?.count ?? 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let  cell = tableView.dequeueReusableCell(withIdentifier: "ExchaneDetailsTableViewCell") as? ExchaneDetailsTableViewCell
        if let ccell = cell {
         
            DispatchQueue.main.async {
                
                ccell.setCell(item: (self.filteredExchanges?[indexPath.row]) ?? ValuteValue(code: "L", value: 1.0))
            }
            
            return ccell
        }
        return  UITableViewCell()
    }
    
    
}
