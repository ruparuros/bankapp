//
//  ExchangeDetailViewModel.swift
//  BankaApplication
//
//  Created by Uros Rupar on 10/19/23.
//

import Foundation

protocol ExchangeDetailViewModelProtocol {
    func calculateExchangeValue(exchanges: [ValuteValue]?, number: Double) -> [ValuteValue]
    func generateUrl(baseVallute:String, othersValute:[String]) -> String
}
class ExchangeDetailViewModel: ExchangeDetailViewModelProtocol {
    
    
    func calculateExchangeValue(exchanges: [ValuteValue]?, number: Double) -> [ValuteValue]{
        return exchanges!.map{ valute in
            let calculatedAmount = valute.value * number
            let object = ValuteValue(code: valute.code, value: calculatedAmount)
            return object
        }
    }
    
    func generateUrl(baseVallute:String, othersValute:[String]) -> String {
        var valutes = ""
        
        for (index,item) in othersValute.enumerated() {
            valutes += item
            if index != othersValute.count - 1 {
                valutes += "%2C"
            }
        }
        
        return valutes
    }
}
