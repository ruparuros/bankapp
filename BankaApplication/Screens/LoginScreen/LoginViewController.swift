//
//  LoginViewController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 2/17/23.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginLabel: UILabel!
    
    @IBOutlet weak var forgotPassword: UIButton!
    @IBOutlet weak var privacyPolicy: UIButton!
    @IBOutlet weak var termsAndConditions: UIButton!
    
    @IBOutlet weak var loginIMage: UIImageView!
    
    var loginViewModel: LoginViewModelProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class var identifier: String { "LoginViewController" }
    
    class func instantiate(loginViewModel: LoginViewModelProtocol) -> LoginViewController {
        
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        
        let vc = storyboard.instantiateViewController(identifier: identifier) as! LoginViewController
        vc.loginViewModel = loginViewModel
        return vc
    }
    
    @IBAction func goToLink(_ sender: Any) {
        loginViewModel.goToWebPage()
    }
    
    func setUI(){
        self.view.backgroundColor = .backgroungGreen
        UITextField.appearance().font = UIFont(name: "UIConst.kFontPoppinsBoldName", size: 13)
        self.navigationController?.navigationBar.tintColor = .white
        self.loginLabel.styleForLoginLabel(title:"Login title".localized())
        self.usernameTextfield.setLoginFields()
        self.passwordTextField.setLoginFields()
        self.loginButton.styleForMainButton(title: "Login title".localized())
        setButtons()
        usernameTextfield.setPlaceHolderText(placeholder:"username placeholder".localized())
        passwordTextField.setPlaceHolderText(placeholder:"password placeholder".localized())
    }
    
    func setButtons(){
        forgotPassword.styleSorConditionsLabel(title: "forgot password")
        privacyPolicy.styleSorConditionsLabel(title: "privacy police")
        termsAndConditions.styleSorConditionsLabel(title: "terms and conditions")
        forgotPassword.setTitle("forgot Password", for: .normal)
        privacyPolicy.setTitle("privacy polcy", for: .normal)
        termsAndConditions.setTitle("terms and conditions", for: .normal)
    }
    
    @IBAction func handleLogin(_ sender: Any) {
        if !loginViewModel.checkUSername(usernameTextfield: self.usernameTextfield) {
            showAlert(title: "username".localized(), message: "username empty".localized())
        } else if !loginViewModel.checkPassword(passwordField: passwordTextField) {
            showAlert(title: "password".localized(), message: "wrong password".localized())
        } else {
            let vc = VerificationViewController.instantiate(verificationViewModel: VerificationViewModel())
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
