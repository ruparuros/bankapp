//
//  LoginViewModel.swift
//  BankaApplication
//
//  Created by Uros Rupar on 10/18/23.
//

import Foundation
import UIKit

protocol LoginViewModelProtocol {
    func checkUSername(usernameTextfield: UITextField) -> Bool
    func checkPassword(passwordField: UITextField) -> Bool
    func goToWebPage()
}

class LoginViewModel: LoginViewModelProtocol {
    func checkUSername(usernameTextfield: UITextField) -> Bool{
        guard let username = usernameTextfield.text else { return false }
        return username.count > 0
    }
    
    func checkPassword(passwordField: UITextField) -> Bool {
        guard let password = passwordField.text else { return false }
        return password.count < 6 || password.isNumeric ? false : true
    }
    
    func goToWebPage(){
       guard let url = URL(string: "https://stackoverflow.com") else { return }
       UIApplication.shared.open(url)
   }
}
