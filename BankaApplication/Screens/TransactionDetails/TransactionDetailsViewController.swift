
import UIKit

class TransactionDetailsViewController: UIViewController {

    @IBOutlet weak var scrolView: UIScrollView!
    @IBOutlet weak var mainstackView: UIStackView!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var statuseValue: UILabel!
    @IBOutlet weak var SenderAccount: UILabel!
    @IBOutlet weak var senderAccountValue: UILabel!
    @IBOutlet weak var receiverAccount: UILabel!
    @IBOutlet weak var receiverAccountValue: UILabel!
    @IBOutlet weak var receiverAddress: UILabel!
    @IBOutlet weak var receiverAddressValue: UILabel!
    @IBOutlet weak var receiverCity: UILabel!
    @IBOutlet weak var receiverCityValue: UILabel!
    @IBOutlet weak var paymentReason: UILabel!
    @IBOutlet weak var paymentReasonValue: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var receiverAccountNumber: UILabel!
    @IBOutlet weak var receiverAccountNumberValue: UILabel!
    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var modelVAlue: UILabel!
    @IBOutlet weak var referencNumber: UILabel!
    @IBOutlet weak var referencNumberVAlue: UILabel!
    @IBOutlet weak var payAgainButton: UIButton!
    
    var transaction: Transaction!
    var userService: UserService!
    class var identifier: String { "TransactionDetailsViewController" }
    
    class func instantiate(transaction: Transaction, userService: UserService) -> TransactionDetailsViewController {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        
        let vc = storyboard.instantiateViewController(identifier: identifier) as! TransactionDetailsViewController
        vc.transaction = transaction
        vc.userService = userService
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    func setUI() {
        self.status.text = "status".localized()
        self.statuseValue.text = self.transaction.status.rawValue
        self.SenderAccount.text = "senderAccount".localized()
        self.senderAccountValue.text = "\(userService.currentUser!.fisrtName) \(userService.currentUser!.lastName) "
        self.receiverCity.text = "recieverCity".localized()
        self.receiverCityValue.text = transaction.recieverCity
        self.receiverAccount.text = "recieverName".localized()
        self.receiverAccountValue.text = transaction.recieverName
        self.receiverAddress.text = "recieverAddress".localized()
        self.receiverAddressValue.text = transaction.recieverAddress
        self.receiverAccountNumber.text = "recieverAccountNumber".localized()
        self.receiverAccountNumberValue.text = transaction.accountNUmber
        self.model.text = "model".localized()
        self.modelVAlue.text = String(transaction.model)
        self.referencNumber.text = "referenceNumber".localized()
        self.referencNumberVAlue.text = transaction.referenceNumber
        self.paymentReason.text = "paymentReason".localized()
        self.paymentReasonValue.text = transaction.paymentReason.reasonTuple.1
        self.amount.text = "amount".localized()
        self.amountLabel.text = String(transaction.amount)
        payAgainButton.styleForPayAgainButton(title: "Pay again")
        setNavigaton()
        setBackgroundGrayLight()
        title = transaction.Id
        let stackViews = mainstackView.subviews(of: UIStackView.self)
        for view in stackViews {
            if view.axis == .vertical {
                view.layer.borderColor = UIColor.backgroungGreen.cgColor
                view.layer.borderWidth = 1
                view.layer.cornerRadius = 9
            }
        }
        
        for (index,label) in mainstackView.subviews(of: UILabel.self).enumerated() {
            
            if index % 2 == 0 {
                label.styleForRegularTextNumber(fontSize: 11)
                label.textColor = .backgroungGreen
            } else {
                label.styleForFontJuaRegular(fontSize: 15)
            }
        }
    }
}
