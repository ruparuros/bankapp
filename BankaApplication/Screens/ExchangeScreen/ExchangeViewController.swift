//
//  ExchangeViewController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 7/16/23.
//

import UIKit

class ExchangeViewController: UIViewController {
    @IBOutlet weak var exchangeTableView: UITableView!
    let datePicker = UIDatePicker()
    var selectedDate: Date = Calendar.current.date(byAdding: .day, value: -1, to: Date()) ?? Date()
    static var currenciesArry: Currencies!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "exchange".localized()
        let nibDate = UINib(nibName: "ExchangeDateTableViewCell", bundle: nil)
        exchangeTableView.register(nibDate, forCellReuseIdentifier: "ExchangeDateTableViewCell")
        let nibValute = UINib(nibName: "ExchangeValuteTableViewCell", bundle: nil)
        exchangeTableView.register(nibValute, forCellReuseIdentifier: "ExchangeValuteTableViewCell")
        exchangeTableView.sectionHeaderTopPadding = 0
        exchangeTableView.bounces = false
        setBackgroundGrayLight()
        setNavigaton()
    }
    
    @objc func dateChanged(datePicker: UIDatePicker){
        selectedDate = datePicker.date
        print(selectedDate)
        print(selectedDate.asString(format: "yyyy-MM-dd"))
    }
}

extension ExchangeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        } else {
            return ExchangeViewController.currenciesArry.values.count       }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let  cell = exchangeTableView.dequeueReusableCell(withIdentifier: "ExchangeDateTableViewCell") as? ExchangeDateTableViewCell
            
            cell?.pickerDate.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
            return cell ?? UITableViewCell()
        }
        
        let cell = exchangeTableView.dequeueReusableCell(withIdentifier: "ExchangeValuteTableViewCell") as? ExchangeValuteTableViewCell
        let ll = ExchangeViewController.currenciesArry.data.keys
        let asss = Array(ll)
        cell?.ValuteLabel.text = ExchangeViewController.currenciesArry.values[indexPath.row].code
        cell?.ValuteLabel.textColor = .red
        return cell ?? UITableViewCell()
    }
}

extension ExchangeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        view.backgroundColor = .backgroungGraylightt
        var titleLabel = UILabel()
        titleLabel.text = section == 0 ? "Rates Date" : "For"
        titleLabel.styleForExpandInfoFormat()
        titleLabel.frame.size.height = 40
        titleLabel.sizeToFit()
        titleLabel.center.y = view.frame.size.height / 2
        titleLabel.frame.origin.x = 10
        titleLabel.textColor = .backgroungGreen
        view.addSubview(titleLabel)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !(indexPath.section == 0) {
            let vc = ExchangeDetailsScreenViewController.instantiate(exchangeDetailsViewModel: ExchangeDetailViewModel(), selectedDate: self.selectedDate, selectedValute: ExchangeViewController.currenciesArry.values[indexPath.row])
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

