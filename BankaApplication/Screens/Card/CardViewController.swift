//
//  CardViewController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 4/1/23.
//

import UIKit

class CardViewController: UIViewController {
    
    @IBOutlet weak var cardsTableView: UITableView!
    
    var cardViewModel = CardViewModel.shared
    var cards: [Card]!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Cards title".localized()
        let nib = UINib(nibName: "CardCell", bundle: nil)
        cardsTableView.register(nib, forCellReuseIdentifier: "CardCell")
        cards = cardViewModel.getCards()
        cardsTableView.separatorStyle = .none
        setNavigaton()
        setBackgroundGrayLight()
    }
}

extension CardViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as? CardCell
        
        cell?.createCell(card: cards[indexPath.row])
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
    
    
}

extension CardViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
}
