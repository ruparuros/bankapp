//
//  CardViewModel.swift
//  BankaApplication
//
//  Created by Uros Rupar on 4/2/23.
//

import Foundation

class CardViewModel {
    static let shared = CardViewModel()
   
    private init(){
    }
    
    let userService = UserService.shared
    
    func getCards() -> [Card]{
        var cards:[Card] = []
        
        userService.getCurrentUser()?.accounts.forEach({ account in
            cards += account.cards
        })
        
        return cards
    }

}
