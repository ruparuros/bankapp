//
//  UILabel.swift
//  BankaApplication
//
//  Created by Uros Rupar on 2/10/23.
//

import Foundation
import UIKit

extension UILabel {
    
    func maintTitle() {
        
    }
    
    func versionText() {
        
    }
    
    func styleForExpandInfoFormat() {
        self.font = UIFont(name: UIConst.kFontJuaRegularName, size: UIConst.kExpandInfoCollapsedFormatNameFontSize) ?? UIFont.systemFont(ofSize: UIConst.kExpandInfoCollapsedFormatNameFontSize)
        self.textColor = UIColor.white
    }
    
    func styleForRegularText() {
        self.font = UIFont(name: UIConst.kFontPoppinsBoldName, size: UIConst.kregularTextFontSize) ?? UIFont.systemFont(ofSize: UIConst.kregularTextFontSize)
        self.textColor = UIColor.white
    }
    
    func styleForRegularTextNumber(fontSize: CGFloat) {
        self.font = UIFont(name: UIConst.kFontPoppinsBoldName, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        self.textColor = UIColor.white
    }
    
    func styleForLoginLabel(title:String) {
        
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:  UIFont(name: UIConst.kFontPoppinsBoldName, size: UIConst.kLoginLabel),
            NSAttributedString.Key.foregroundColor: UIColor.backgroungGreen
          ]
        let attributeString = NSMutableAttributedString(
            string: title,
            attributes: atributedString
            )
        self.attributedText = attributeString
        
        self.font = UIFont(name: UIConst.kFontPoppinsBoldName, size: UIConst.kLoginLabel) ?? UIFont.systemFont(ofSize: UIConst.kLoginLabel)
        self.textColor = UIColor.white
        self.textAlignment = .center
    }
    
    func styleForAccountNumberLabel() {
        
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:  UIFont(name: UIConst.kFontJuaRegularName, size: UIConst.kLoginLabel),
            NSAttributedString.Key.foregroundColor: UIColor.backgroungGreen
          ]
        let attributeString = NSMutableAttributedString(
            string: self.text ?? "",
            attributes: atributedString
            )
        self.attributedText = attributeString
        
        self.font = UIFont(name: UIConst.kFontJuaRegularName, size: UIConst.kLoginLabel) ?? UIFont.systemFont(ofSize: UIConst.kLoginLabel)
        self.textColor = UIColor.backgroungGreen
        self.textAlignment = .left
    }
    
    func styleForAccountNumberLabel(size: CGFloat) {
        
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:  UIFont(name: UIConst.kFontJuaRegularName, size: size),
            NSAttributedString.Key.foregroundColor: UIColor.backgroungGreen
          ]
        let attributeString = NSMutableAttributedString(
            string: self.text ?? "",
            attributes: atributedString
            )
        self.attributedText = attributeString
        
        self.font = UIFont(name: UIConst.kFontJuaRegularName, size: size) ?? UIFont.systemFont(ofSize: size)
        self.textColor = UIColor.backgroungGreen
        self.textAlignment = .left
    }
    
    func styleForAccountNumberBalance() {
        
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:  UIFont(name: UIConst.kFontJuaRegularName, size: UIConst.kBalanceLabel),
            NSAttributedString.Key.foregroundColor: UIColor.backgroungGreen
          ]
        let attributeString = NSMutableAttributedString(
            string: self.text ?? "",
            attributes: atributedString
            )
        self.attributedText = attributeString
        
        self.font = UIFont(name: UIConst.kFontJuaRegularName, size: UIConst.kBalanceLabel) ?? UIFont.systemFont(ofSize: UIConst.kBalanceLabel)
        self.textColor = UIColor.backgroungGreenLight
        self.textAlignment = .left
    }
    
    func styleForFontJuaRegular(fontSize:CGFloat) {
        
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:  UIFont(name: UIConst.kFontJuaRegularName, size: fontSize),
            NSAttributedString.Key.foregroundColor: UIColor.backgroungGreen
          ]
        let attributeString = NSMutableAttributedString(
            string: self.text ?? "",
            attributes: atributedString
            )
        self.attributedText = attributeString
        
        self.font = UIFont(name: UIConst.kFontJuaRegularName, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        self.textColor = UIColor.backgroungGreen
    }
    
    func styleForIBMPlexMonoSemiBold(fontSize:CGFloat) {
        self.font = UIFont(name: UIConst.kIBMPlexMonoSemiBold, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        self.textColor = UIColor.backgroungGreen
    }
}
