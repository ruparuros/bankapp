//
//  UITextField.swift
//  BankaApplication
//
//  Created by Uros Rupar on 2/17/23.
//

import Foundation
import UIKit

extension UITextField {
    
    func setLoginFields(){
        self.backgroundColor = .white
        self.layer.cornerRadius = UIConst.kCornerRadius
        self.textColor = .backgroungGreen
    }
    
    func setInputAmountField(){
        self.backgroundColor = .white
        self.layer.cornerRadius = UIConst.kCornerRadius
        self.layer.borderWidth = UIConst.kBorderWidth
        self.layer.borderColor = UIColor.backgroungGreen.cgColor
        self.layer.masksToBounds = true
        self.textColor = .backgroungGreen
    }
    
    func setPlaceHolderText(placeholder: String){
        self.text = nil
        self.placeholder = placeholder
        
            self.attributedPlaceholder =
        NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.textAlignment = .center
    }
    
    func setPlaceHolderTextPayment(placeholder: String, size: CGFloat){
      
        self.placeholder = placeholder
        self.font = UIFont(name: UIConst.kFontJuaRegularName, size: size)
        self.textColor = .backgroungGreen
        self.autocapitalizationType = .sentences
      
        
            
    }
    

}
