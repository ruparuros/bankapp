//
//  UIView.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/30/23.
//

import Foundation
import UIKit

extension UIView {
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            return UIView()
        }
        return view
    }
    
    func dropShadow(scale: Bool = true) {
      layer.masksToBounds = true
      layer.shadowColor = UIColor.black.cgColor
      layer.shadowOpacity = 0.5
      layer.shadowOffset = CGSize(width: 1, height: 1)
        layer.shadowRadius = 0.5
    }
    
    func getAllSubViews() -> [UIView] {
        return subviews + subviews.flatMap { $0.getAllSubViews() }
    }
    
    func subviews<T>(of type: T.Type) -> [T] {
        return getAllSubViews().compactMap { $0 as? T }
    }
}
