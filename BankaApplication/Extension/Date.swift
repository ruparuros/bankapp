//
//  Date.swift
//  BankaApplication
//
//  Created by Uros Rupar on 10/18/23.
//

import Foundation

extension Date {
    func asString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en")

        return dateFormatter.string(from: self)
    }
}
