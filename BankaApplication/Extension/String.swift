//
//  String.swift
//  BankaApplication
//
//  Created by Uros Rupar on 2/24/23.
//

import Foundation

extension String {
    var isNumeric: Bool {
        return !(self.isEmpty) && self.allSatisfy { $0.isNumber }
      }
    
    func localized(withLanguageFrom userDefaults: UserDefaults = UserDefaults.standard, argument: String? = nil) -> String {
       
        var path = Bundle.main.path(forResource: "en", ofType: "lproj")
        if path == nil {
            path = Bundle.main.path(forResource: "hr", ofType: "lproj")
        }
        let bundle = Bundle(path: path!)
        
        let localizedString = NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        if let argument = argument {
            return String(format: localizedString, argument)
        } else {
            return localizedString
        }
    }
    
    func generateSecureText() -> String {
        let chars = self.reduce(into: "") { result, char in
            result += "*"
        }
        return chars
    }
}
