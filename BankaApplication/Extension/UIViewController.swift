//
//  UIViewController.swift
//  BankaApplication
//
//  Created by Uros Rupar on 10/18/23.
//

import Foundation
import UIKit

extension UIViewController {
    func setBackgroundGrayLight(){
        self.view.backgroundColor = .backgroungGraylightt
    }
    
    func setNavigaton() {
        self.navigationController?.navigationBar.backgroundColor = .backgroungGreen
        self.navigationController?.navigationBar.barTintColor = .backgroungGreen
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        setStatusBarColor()
        setRightSearchButton(action: #selector(goToAccountScreen), target: self, image: "person.crop.circle")
    }
    
    func setStatusBarColor(){
        guard let frame = navigationController?.navigationBar.bounds else { return }
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        let statusBarGradient = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: statusBarHeight))
        statusBarGradient.backgroundColor = .backgroungGreen
        view.addSubview(statusBarGradient)
    }
    
    func setRightSearchButton(action: Selector? = nil, target: AnyObject? = nil, image: String ) {
        let rightButton = UIBarButtonItem(title: "", style: .plain, target: target, action: action)
        rightButton.image = UIImage(systemName: image)
        rightButton.tintColor = .white
        self.navigationItem.setRightBarButton(rightButton, animated: true)
    }
    
    func showAlert(title: String,message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func goToAccountScreen() {
        let vc = AccountViewController.instantiate(userService: UserService.shared) // singltone object of User Service
        self.present(vc, animated: true)
    }

}
