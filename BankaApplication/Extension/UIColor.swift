//
//  UIColor.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/19/23.
//

import Foundation
import UIKit

extension UIColor {
    
    static func colorWithRGBValues(redValue: CGFloat, greenValue: CGFloat, blueValue: CGFloat, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: alpha)
    }
    
    public class var backgroungGreen: UIColor {
        return UIColor.colorWithRGBValues(redValue: 65, greenValue: 104, blueValue: 145)
    }
    
    public class var backgroungGreenLight: UIColor {
        return UIColor.colorWithRGBValues(redValue: 119, greenValue: 147, blueValue: 177)
    }
    
    public class var backgroungGraylightt: UIColor {
        return UIColor.colorWithRGBValues(redValue: 236, greenValue: 240, blueValue: 244)
    }
    
}
