//
//  UIButton.swift
//  BankaApplication
//
//  Created by Uros Rupar on 2/10/23.
//

import Foundation
import UIKit

extension UIButton {
    
    func styleForMainButton(title: String) {
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:  UIFont(name: UIConst.kFontPoppinsBoldName, size: UIConst.kregularTextFontSize),
            NSAttributedString.Key.foregroundColor: UIColor.backgroungGreen
          ]
        let attributeString = NSMutableAttributedString(
            string: title,
            attributes: atributedString
            )
        
        self.setAttributedTitle(attributeString, for: .normal)
        self.backgroundColor = .white  
        self.layer.cornerRadius = UIConst.kCornerRadius
        self.layer.masksToBounds = true
        self.frame.size = CGSize(width: 100, height: 37)
    }
    
    func styleSorConditionsLabel(title: String) {
        self.contentHorizontalAlignment = .left
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:  UIFont(name: UIConst.kFontPoppinsBoldName, size: UIConst.kConditionsLabelSize),
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
          ]
        let attributeString = NSMutableAttributedString(
              string: title,
              attributes: atributedString
            )
        
            self.setAttributedTitle(attributeString, for: .normal)
        self.contentHorizontalAlignment = .left
    }
    
    func styleForResendButton(title: String) {
        self.contentHorizontalAlignment = .left
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:  UIFont(name: UIConst.kFontPoppinsBoldName, size: UIConst.kConditionsLabelSize),
            NSAttributedString.Key.foregroundColor: UIColor.white
          ]
        let attributeString = NSMutableAttributedString(
              string: title,
              attributes: atributedString
            )
        
            self.setAttributedTitle(attributeString, for: .normal)
        self.contentHorizontalAlignment = .left
    }
    
    func styleForLogoutButton(title: String) {
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:  UIFont(name: UIConst.kFontPoppinsBoldName, size: 20),
            NSAttributedString.Key.foregroundColor: UIColor.red
          ]
        let attributeString = NSMutableAttributedString(
            string: title,
            attributes: atributedString
            )
        
        self.setAttributedTitle(attributeString, for: .normal)
        self.backgroundColor = .white
        self.layer.cornerRadius = UIConst.kCornerRadius
        self.layer.masksToBounds = true
        self.frame.size = CGSize(width: 100, height: 37)
    }

    func styleForPayAgainButton(title: String) {
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:  UIFont(name: UIConst.kFontPoppinsBoldName, size: UIConst.kregularTextFontSize),
            NSAttributedString.Key.foregroundColor: UIColor.white
          ]
        let attributeString = NSMutableAttributedString(
            string: title,
            attributes: atributedString
            )
        
        self.setAttributedTitle(attributeString, for: .normal)
      
        self.backgroundColor = .backgroungGreen
        self.layer.cornerRadius = UIConst.kCornerRadius
        self.layer.masksToBounds = true
        
    }
}
