//
//  AccountTableHeaderView.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/30/23.
//

import UIKit

class AccountTableHeaderView: UIView {

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var creditCardLogoStackView: UIStackView!
    
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
        
    let nibName = "AccountTableHeaderView"
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        let view = self.loadNib()
        view.frame = self.bounds
        self.addSubview(view)
        setView()
    }
    
   func setView(){
       self.backgroundColor = .clear
      // wrapperView.backgroundColor = .white
       wrapperView.layer.cornerRadius = 10
       wrapperView.clipsToBounds = true
       wrapperView.layer.masksToBounds = false
       wrapperView.layer.shadowRadius = 2
       wrapperView.layer.shadowOpacity = 0.68
       wrapperView.layer.shadowOffset = CGSize(width: 1, height: 1)
       wrapperView.layer.shadowColor = UIColor.black.cgColor
}
    
    func createHeaderView(account:Account){
        balanceLabel.text = String(account.balance)
        balanceLabel.styleForAccountNumberBalance()
        numberLabel.text = String(account.number)
        numberLabel.styleForAccountNumberLabel()
        
        for view in self.creditCardLogoStackView.subviews where view is UIImageView {
            view.removeFromSuperview()
        }
       // creditCardLogoStackView.removeAllArrangedSubviews()
        for card in account.cards {
            
            let ImageView = UIImageView(image: card.category.image)
            ImageView.translatesAutoresizingMaskIntoConstraints = true
            
            NSLayoutConstraint.activate([
                ImageView.widthAnchor.constraint(equalToConstant: 25.0),
                ImageView.heightAnchor.constraint(equalToConstant: 17.0)
            ])
            self.creditCardLogoStackView.addArrangedSubview(ImageView)
         
        }
    }
}

