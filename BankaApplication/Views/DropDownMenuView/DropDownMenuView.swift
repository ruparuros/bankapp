//
//  DropDownMenuView.swift
//  BankaApplication
//
//  Created by Uros Rupar on 10/27/23.
//

import UIKit

 protocol DropDownMenuViewDelegate {
     func onSelectedOrEnteredValue<T>(dropDownMenuView:DropDownMenuView, item:T , index: Int?)
}

class DropDownMenuView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var placeholderTextLabel: UILabel!
    @IBOutlet weak var ammountLabel: UILabel!
    @IBOutlet weak var hStackView: UIStackView!
    var delegate: DropDownMenuViewDelegate?
    
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        let view = self.loadNib()
        view.frame = bounds
        self.addSubview(view)
        setUI()
    }

    func setUI() {
        self.layer.borderColor = UIColor.backgroungGreen.cgColor
        self.layer.borderWidth = 2
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        titleLabel.styleForRegularTextNumber(fontSize: 12)
        placeholderTextLabel.styleForAccountNumberLabel(size: 20)
        ammountLabel.styleForAccountNumberLabel(size: 20)
        titleLabel.textColor = .backgroungGreen
        placeholderTextLabel.textColor = .backgroungGreen
        menuButton.tintColor = .backgroungGreen
        inputTextField.borderStyle = .none
        inputTextField.setPlaceHolderTextPayment(placeholder: "",size: 18)
            }
    
    func setMenu(placeholderText: String, titles: [String]) {
        inputTextField.isHidden = true
        ammountLabel.isHidden = true
        placeholderTextLabel.text = placeholderText
        titleLabel.text = "Payment reason"
        
        var elements: [UIAction] = []
        for (index, item) in titles.enumerated() {
            elements.append(UIAction(title: item ,handler: { action in
                self.placeholderTextLabel.text = item
                self.delegate?.onSelectedOrEnteredValue(dropDownMenuView: self, item:item, index: index)
            }))
        }
        let menu = UIMenu(children: elements)
        
        menuButton.menu = menu
        menuButton.showsMenuAsPrimaryAction = true
        
    }
    
    func setMenuAccounts(placeholderText: String, accounts: [Account], amount: String? = nil) {
        inputTextField.isHidden = true
        placeholderTextLabel.text = placeholderText
        titleLabel.text = "fromaAccount".localized()
        ammountLabel.text = amount
        var elements: [UIAction] = []
        for (index, item) in accounts.enumerated() {
            elements.append(UIAction(title: item.title ,handler: { action in
                self.placeholderTextLabel.text = item.title
                self.ammountLabel.text = "\(item.balance) \(item.currency.sign)"
                self.delegate?.onSelectedOrEnteredValue(dropDownMenuView: self, item:item, index: index)
            }))
        }
        let menu = UIMenu(children: elements)
        
        menuButton.menu = menu
        menuButton.showsMenuAsPrimaryAction = true
      
    }
    
    func setTextFieldType(placeholder: String) {
        hStackView.isHidden = true
        ammountLabel.isHidden = true
        placeholderTextLabel.isHidden = true
        self.titleLabel.text = placeholder
        
    }
    
    
 
}
