//
//  Bank.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/30/23.
//

import Foundation

struct Bank {
    var id: String
    var title:String
    
    init(title: String) {
        self.id = Utils.getRandomNumber(digits: 3)
        self.title = title
    }
}
