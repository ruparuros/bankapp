//
//  Currencies.swift
//  BankaApplication
//
//  Created by Uros Rupar on 9/26/23.
//

import Foundation

struct Currencies: Codable {
    let data: [String: CAD]
    var values: [CAD] {
        
        return Array(data.values)
    }
}


// MARK: - CAD
struct CAD: Codable {
    let symbol, name, symbolNative: String?
    let decimalDigits, rounding: Int?
    let code, namePlural: String?

    enum CodingKeys: String, CodingKey {
        case symbol, name
        case symbolNative = "symbol_native"
        case decimalDigits = "decimal_digits"
        case rounding, code
        case namePlural = "name_plural"
    }
}
