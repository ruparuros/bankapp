//
//  Account.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/27/23.
//

import Foundation
import UIKit

class Account {
    
    var user:User?
    var Id: String?
    var title: String {
        return category.description + " account"
    }
    var number: String
    private var isActive: Bool
    var category: Category
    var transactions: [Transaction]
    var currency: Currency
    var bank: Bank
    var balance: Double  {
        return transactions
            .map{ $0.amount }
            .reduce(0,+)
    }
    var cards:[Card]
    
    init (category: Category, currency: Currency, bank: Bank, number: String) {
        self.Id = Utils.getRandomNumber(digits: 5)
        self.isActive = true
        self.category = category
        self.transactions = []
        self.currency = currency
        self.bank = bank
        self.number = number
        self.cards = []
        self.user = nil
    }
    
    func activateAccount(){
        self.isActive = true
    }
    
    func deactivateAccount(){
        self.isActive = false
    }
    
    func addTransaction(transaction: Transaction) {
        self.transactions.append(transaction)
    }
    
    func addTransactions(transactions: [Transaction]) {
        self.transactions += transactions
    }
    
    func appendCard(category:CardCategory){
        cards.append(Card(account: self, category: category))
    }
}

enum Category {
    case deposit
    case savings
    case loan
    
    var description : String {
      switch self {

      case .deposit: return "Deposit"
      case .savings: return "Savings"
      case .loan: return "Loan"
      }
    }
}

enum Currency {
    case eur
    case usd
    case rsd
    case rub
    
    var sign : String {
      switch self {

      case .eur: return "€"
      case .usd: return "$"
      case .rsd: return "rsd"
      case .rub: return "rub"
      }
    }
}

enum CardCategory {
    case unknown
    case Visa
    case Mastercard
    case American
    case Express
    case Diners
    case Club
    case Discover
    case JCB
    case China
    case UnionPay
    
    var image : UIImage {
      switch self {

      case .Visa: return UIImage(named: "visalogo") ?? UIImage()
      case .Mastercard: return UIImage(named: "masterCardLogo") ?? UIImage()
      case .unknown:
          return UIImage(named: "visalogo") ?? UIImage()
      case .American:
          return UIImage(named: "masterCardLogo") ?? UIImage()
      case .Express:
          return UIImage(named: "visalogo") ?? UIImage()
      case .Diners:
          return UIImage(named: "masterCardLogo") ?? UIImage()
      case .Club:
          return UIImage(named: "visalogo") ?? UIImage()
      case .Discover:
          return UIImage(named: "masterCardLogo") ?? UIImage()
      case .JCB:
          return UIImage(named: "visalogo") ?? UIImage()
      case .China:
          return UIImage(named: "masterCardLogo") ?? UIImage()
      case .UnionPay:
          return UIImage(named: "visalogo") ?? UIImage()
      }
    }
}
