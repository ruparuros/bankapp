//
//  User.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/27/23.
//

import Foundation

class User {
    
    var Id: String
    var fisrtName: String
    var lastName: String
    var birthDay: Date
    var phoneNumber: String
    var email: String
    var password: String
    var address: (city: String?, street: String?, number: String?,postalCode: String?)
    var accounts: [Account]
    
    init(fisrtName: String, lastname: String, birthDay: Date, email:String, password: String, addres: (city: String?, street: String?, number: String?,postalCode: String?), accounts: [Account] = [], cards:[Card] = []) {
        self.Id = Utils.getRandomNumber(digits: 6)
        self.fisrtName = fisrtName
        self.lastName = lastname
        self.birthDay = birthDay
        self.phoneNumber = Utils.getRandomNumber(digits: 9)
        self.email = email
        self.password = password
        self.address = addres
        self.accounts = []
     }
    
    
    func appendAccount(account: Account){
        account.user = self
        accounts.append(account)
    }
}


