//
//  Card.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/27/23.
//

import Foundation

struct Card {
    
    init(account: Account, category: CardCategory) {
        self.id = Utils.getRandomNumber(digits: 6)
        self.number = Utils.getRandomNumber(digits: 16)
        self.validFrom = Date()
        self.validTo = Date()
        self.cvv = Utils.getRandomNumber(digits: 3)
        self.category = category
        self.account = account
    }
    var account: Account
    var id: String
    var holderName: String {
        return  "\(String(account.user?.fisrtName ?? "").prefix(1).uppercased())  \(account.user?.lastName ?? "".uppercased())"
    }
    var number: String
    let validFrom:Date
    let validTo: Date
    let cvv: String
    var category: CardCategory
    
}


