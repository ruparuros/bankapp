//
//  Transaction.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/27/23.
//

import Foundation
import UIKit

struct Transaction {

        
    var Id: String
    var date: Date
    var amount: Double
    var status: Status
    var kind: Kind
    var recieverName: String
    var recieverAddress: String
    var recieverCity: String
    var accountNUmber: String
    var paymentReason: Reason
    var model: Int
    var referenceNumber: String
    var isUrgent: Bool
    
    
    internal init(amount: Double, status: Status, kind: Kind, recieverName: String, recieverAddress: String, paymentReason: Reason, model: Int, isUrgent: Bool) {
        self.Id = Utils.getRandomNumber(digits: 3)
        self.date = Date()
        self.amount = amount
        self.status = status
        self.kind = kind
        self.recieverName = recieverName
        self.recieverAddress = recieverAddress
        self.recieverCity = "Belgrade"
        self.accountNUmber = "\(Utils.getRandomNumber(digits: 2)) - \(Utils.getRandomNumber(digits: 7)) - \(Utils.getRandomNumber(digits: 2))"
        self.paymentReason = paymentReason
        self.model = model
        self.referenceNumber = "\(Utils.getRandomNumber(digits: 2)) - \(Utils.getRandomNumber(digits: 7)) - \(Utils.getRandomNumber(digits: 2))"
        self.isUrgent = isUrgent
    }
}

enum Status: String {
    case pending
    case processing
    case processed
    case canceled
}

enum Kind {
    case outgoing
    case incoming
}

enum Reason: CaseIterable {
    case reason100
    case reason200
    case reason283
    case reason321
    
    var reasonTuple: (Int,String) {
        switch self {
        case .reason100:
            return (100, "New Year present")
        case .reason200:
            return (200, "Internet provider fee")
        case .reason283:
            return (183, "Mobile provider fee")
        case .reason321:
            return (321, "Regular user to user transaction - 404 - Salary yay")
        }
    }
}
