//
//  ConvertCurrency.swift
//  BankaApplication
//
//  Created by Uros Rupar on 9/27/23.
//

import Foundation

// MARK: - ConvertCurrenciy
struct ConvertCurrenciy: Codable {
    let meta: MetaData
    let data: [String: ValuteValue]
    var values: [ValuteValue] {
        return Array(data.values)
    }
}

struct ValuteValue: Codable {
    let code: String
    var value: Double
    var amountStringValue: String {
        String(format: "%.2f", value)
    }
}

// MARK: - Meta
struct MetaData: Codable {
    let lastUpdatedAt: String

    enum CodingKeys: String, CodingKey {
        case lastUpdatedAt = "last_updated_at"
    }
}
