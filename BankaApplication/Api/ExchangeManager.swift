//
//  ExchangeManager.swift
//  BankaApplication
//
//  Created by Uros Rupar on 9/18/23.
//

import Foundation

protocol ExchangeManagerProtocol {
    static func fetchConfig<T: Decodable>(url: String, type: T.Type, completion: @escaping (T?) -> Void)
}

class ExchangeManager: ExchangeManagerProtocol {
    
   static func fetchConfig<T: Decodable>(url: String, type: T.Type, completion: @escaping (T?) -> Void) {
        
        guard let url = URL(string: url) else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let features = try JSONDecoder().decode(T.self, from: data)
                completion(features)
            } catch {
                completion(nil)
            }
        }.resume()
    }
    
   static func fetchRemoteConfigg(url: String, completion: @escaping (Currencies?) -> Void) {
        guard let url = URL(string: url) else {
            return
        }

        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let features = try JSONDecoder().decode(Currencies.self, from: data)
                completion(features)
            } catch {
                completion(nil)
            }
        }.resume()
    }
}
