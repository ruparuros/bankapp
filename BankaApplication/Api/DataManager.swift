//
//  DataManager.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/30/23.
//

import Foundation

class DataManager {
    
    
    
    
    static func fetchData() -> User{
        
        var data: [Transaction] = [
              Transaction(
                  amount: 2110.0,
                  status: .processed,
                  kind: .incoming,
                  recieverName: "Jovan Avakumovic",
                  recieverAddress: "Dragana Perisica 21",
                  paymentReason: .reason200,
                  model: 302,
                  isUrgent: true
              ),
              Transaction(
                  amount: 4410.0,
                  status: .canceled,
                  kind: .outgoing,
                  recieverName: "Drakula Mirotic",
                  recieverAddress: "Dragana Perisica 99",
                  paymentReason: .reason283,
                  model: 302,
                  isUrgent: false
              ),
              Transaction(
                  amount: 3210.0,
                  status: .processing,
                  kind: .incoming,
                  recieverName: "Paja Jovanovic",
                  recieverAddress: "Zagrbacka 34",
                  paymentReason: .reason100,
                  model: 301,
                  isUrgent: true
              ),
              Transaction(
                  amount: 2300.0,
                  status: .pending,
                  kind: .outgoing,
                  recieverName: "Milojica Stanic 55",
                  recieverAddress: "Dr Milica Hadzica 1",
                  paymentReason: .reason200,
                  model: 362,
                  isUrgent: false
              ),
              Transaction(
                  amount: 4510.0,
                  status: .processed,
                  kind: .outgoing,
                  recieverName: "Jovan Avakumovic",
                  recieverAddress: "Dragana Perisica 44 c",
                  paymentReason: .reason100,
                  model: 342,
                  isUrgent: true
              ),
              Transaction(
                  amount: 6090.0,
                  status: .processed,
                  kind: .incoming,
                  recieverName: "Hasan Protic",
                  recieverAddress: "Drgoljuba Likica 231 b",
                  paymentReason: .reason321,
                  model: 322,
                  isUrgent: false
              ),
              Transaction(
                  amount: 34000.0,
                  status: .pending,
                  kind: .incoming,
                  recieverName: "Jovan Pastroivc",
                  recieverAddress: " Homoljska 21",
                  paymentReason: .reason283,
                  model: 102,
                  isUrgent: true
              ),
            
              ]
        
        var data1: [Transaction] = [
              Transaction(
                  amount: 20.0,
                  status: .processed,
                  kind: .incoming,
                  recieverName: "Jovan Avakumovic",
                  recieverAddress: "Dragana Perisica 21",
                  paymentReason: .reason200,
                  model: 302,
                  isUrgent: true
              ),
              Transaction(
                  amount: 410.0,
                  status: .canceled,
                  kind: .outgoing,
                  recieverName: "Drakula Mirotic",
                  recieverAddress: "Dragana Perisica 99",
                  paymentReason: .reason283,
                  model: 302,
                  isUrgent: false
              ),
              Transaction(
                  amount: 310.0,
                  status: .processing,
                  kind: .incoming,
                  recieverName: "Paja Jovanovic",
                  recieverAddress: "Zagrbacka 34",
                  paymentReason: .reason100,
                  model: 301,
                  isUrgent: true
              ),
              Transaction(
                  amount: 200.0,
                  status: .pending,
                  kind: .outgoing,
                  recieverName: "Milojica Stanic 55",
                  recieverAddress: "Dr Milica Hadzica 1",
                  paymentReason: .reason200,
                  model: 362,
                  isUrgent: false
              ),
              Transaction(
                  amount: 510.0,
                  status: .processed,
                  kind: .outgoing,
                  recieverName: "Jovan Avakumovic",
                  recieverAddress: "Dragana Perisica 44 c",
                  paymentReason: .reason100,
                  model: 342,
                  isUrgent: true
              ),
              Transaction(
                  amount: 690.0,
                  status: .processed,
                  kind: .incoming,
                  recieverName: "Hasan Protic",
                  recieverAddress: "Drgoljuba Likica 231 b",
                  paymentReason: .reason321,
                  model: 322,
                  isUrgent: false
              ),
              Transaction(
                  amount: 3000.0,
                  status: .pending,
                  kind: .incoming,
                  recieverName: "Jovan Pastroivc",
                  recieverAddress: " Homoljska 21",
                  paymentReason: .reason283,
                  model: 102,
                  isUrgent: true
              ),
            
              ]
        
        var account1 = Account(category: .deposit, currency: .rsd, bank: Bank(title:"Raiffaisen"), number: "23 - 0043455 - 9")
        account1.addTransactions(transactions: data)
        
        var account2 = Account(category: .loan, currency: .eur, bank: Bank(title:"Raiffaisen"), number: "12 - 5594849 - 90")
        account2.addTransactions(transactions: data1)
        
        account1.appendCard(category: .Mastercard)
        account1.appendCard(category: .Visa)
        
        account2.appendCard(category: .Visa)
        
        var user2 = User(fisrtName: "Borivoje", lastname: "Milic", birthDay: Date(), email: "email@gmail.com", password: "mitarMiric", addres: ("Zlatibor","Miliceva","65","11234"))
        
        user2.appendAccount(account: account1)
        user2.appendAccount(account: account2)
       
        
        return user2
    }
    
}
