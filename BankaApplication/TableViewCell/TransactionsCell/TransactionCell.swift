//
//  TransactionsCell.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/27/23.
//

import UIKit

class TransactionsCell: UITableViewCell {
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var recieverNameAndNumber: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var amountOfTransactionsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func createCell(transaction: Transaction, currency:Currency) {
        let date = transaction.date
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        let time = timeFormatter.string(from: date)
        
        let stringDateFormatter = DateFormatter()
        stringDateFormatter.dateFormat = "dd.MM.yyyy"
        stringDateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let day = stringDateFormatter.string(from: date)
        let color: UIColor = transaction.status == .processed ? (transaction.kind == .incoming) ? .green : .red : .backgroungGreenLight
        let sign = transaction.kind == .incoming ? " + " :  " - "
    
        recieverNameAndNumber.text = "\(transaction.recieverName) reason \(transaction.paymentReason.reasonTuple.0)"
        timeLabel.text = "\(day) \(time)"
        recieverNameAndNumber.styleForRegularTextNumber(fontSize: 12)
        timeLabel.styleForRegularTextNumber(fontSize: 12)
        amountOfTransactionsLabel.text = sign + String(transaction.amount).replacingOccurrences(of: ".", with: " ,") + "0  \(currency.sign.uppercased())"
        recieverNameAndNumber.textColor = color
        timeLabel.textColor = color
        amountOfTransactionsLabel.textColor = color
        
        var image = "arrow.down"
        switch transaction.status {
        case .pending:
            image = "clock.arrow.circlepath"
        case .processing:
            image = "hourglass"
        case .processed:
            image = transaction.kind == .incoming ? "arrow.down" : "arrow.up"
        case .canceled:
             image = "slash.circle"
        }
        arrowImage.image = UIImage(systemName: image)
        arrowImage.tintColor = color
    }
    

}
