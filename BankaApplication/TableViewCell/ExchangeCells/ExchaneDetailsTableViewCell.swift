//
//  ExchaneDetailsTableViewCell.swift
//  BankaApplication
//
//  Created by Uros Rupar on 8/11/23.
//

import UIKit

class ExchaneDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var signLabel: UILabel!
    @IBOutlet weak var valuteLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setUI()
        // Initialization code
    }


    func setCell(item: ValuteValue){
        valuteLabel.text = item.code
        amountLabel.text =  item.amountStringValue
    }
    
    func setUI() {
        valuteLabel.textColor = .backgroungGreen
        amountLabel.textColor = .backgroungGreen
        signLabel.textColor = .backgroungGreen
    }
}
