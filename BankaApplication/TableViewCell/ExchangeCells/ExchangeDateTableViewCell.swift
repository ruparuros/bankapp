//
//  ExchangeDateTableViewCell.swift
//  BankaApplication
//
//  Created by Uros Rupar on 7/16/23.
//

import UIKit

class ExchangeDateTableViewCell: UITableViewCell {

    @IBOutlet weak var pickerDate: UIDatePicker!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        setUI()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setUI() {
        pickerDate.sizeToFit()
        pickerDate.minimumDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        pickerDate.maximumDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        pickerDate.setValue(UIColor.white, forKey: "backgroundColor")
        pickerDate.setValue(UIColor.red, forKey:"textColor")
        pickerDate.backgroundColor = .white
    }
    
    func updateDate(date: Date?){
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM dd, yyyy"
        dateFormat.locale = Locale(identifier: "nl")
        
        var parsedDate: String!
        if let date = date {
             parsedDate = dateFormat.string(from: date)
        } else {
             parsedDate = dateFormat.string(from: Date())
        }
  
    }
    

}
