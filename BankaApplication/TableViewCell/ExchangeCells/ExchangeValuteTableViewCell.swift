//
//  ExchangeValuteTableViewCell.swift
//  BankaApplication
//
//  Created by Uros Rupar on 7/16/23.
//

import UIKit

class ExchangeValuteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ValuteLabel: UILabel!
    
    @IBOutlet weak var buttonArrow: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        ValuteLabel.textColor = .backgroungGreen
        buttonArrow.tintColor = .backgroungGreen
        // Configure the view for the selected state
    }

}
