//
//  CardCell.swift
//  BankaApplication
//
//  Created by Uros Rupar on 4/1/23.
//

import UIKit

class CardCell: UITableViewCell {

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var account: UILabel!
    @IBOutlet weak var accountNumber: UILabel!
    @IBOutlet weak var accountBalance: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var validUntilLabel: UILabel!
    @IBOutlet weak var validUntilDateLabel: UILabel!
    @IBOutlet weak var cardHolderNameStringLabel: UILabel!
    @IBOutlet weak var cardHolderLabel: UILabel!
    
    @IBOutlet weak var bankLogoImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setView(){
        
        self.backgroundColor = .clear
        wrapperView.backgroundColor = .white
        wrapperView.layer.cornerRadius = 10
        wrapperView.clipsToBounds = true
        wrapperView.layer.masksToBounds = false
        wrapperView.layer.shadowRadius = 2
        wrapperView.layer.shadowOpacity = 0.68
        wrapperView.layer.shadowOffset = CGSize(width: 1, height: 1)
        wrapperView.layer.shadowColor = UIColor.black.cgColor
 }
    
    func createCell(card:Card){
        account.text = "Account"
        account.styleForIBMPlexMonoSemiBold(fontSize: UIConst.CardCell.kSmallStrings)
        accountNumber.text = card.account.number
        accountNumber.styleForFontJuaRegular(fontSize: UIConst.CardCell.kMediumStrings)
        accountBalance.text = String(card.account.balance)
        accountBalance.styleForIBMPlexMonoSemiBold(fontSize: UIConst.CardCell.kSmallStrings)
        cardNumberLabel.text = card.number
        cardNumberLabel.styleForFontJuaRegular(fontSize: UIConst.CardCell.kLargeStrings)
        
        let stringDateFormatter = DateFormatter()
        stringDateFormatter.dateFormat = "dd / MM / yyyy"
        stringDateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        var date = stringDateFormatter.string(from: card.validTo)
        
        validUntilLabel.text = "Valid until"
        validUntilLabel.styleForIBMPlexMonoSemiBold(fontSize: UIConst.CardCell.kSmallStrings)
        validUntilDateLabel.text = date
        validUntilDateLabel.styleForFontJuaRegular(fontSize: UIConst.CardCell.kMediumStrings)

        cardHolderLabel.text = card.holderName
        cardHolderLabel.styleForFontJuaRegular(fontSize: UIConst.CardCell.kLargeStrings)
        
        cardHolderNameStringLabel.text = "Card holder name"
        cardHolderNameStringLabel.styleForIBMPlexMonoSemiBold(fontSize: UIConst.CardCell.kSmallStrings)
        
        bankLogoImage.image = card.category.image
    }
}
