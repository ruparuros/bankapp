//
//  USerService.swift
//  BankaApplication
//
//  Created by Uros Rupar on 3/30/23.
//

import Foundation

class UserService {
    
    static let shared = UserService()
    var currentUser: User!
    
    private init(){
    }
    
    func login(user:User) -> Bool{
        if currentUser == nil {
            self.currentUser = user
            return true
        } else {
            return false
        }
    }
    
    func logout(){
        currentUser = nil
    }
    
    func getCurrentUser() -> User?{
        return currentUser
    }
    
    
    
}
